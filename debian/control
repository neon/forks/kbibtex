Source: kbibtex
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Bastien Roucariès <rouca@debian.org>, Pino Toscano <pino@debian.org>
Section: kde
Priority: optional
Build-Depends: chrpath,
               cmake (>= 3.7.2~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 5.51.0~),
               gettext,
               libicu-dev,
               libkf5coreaddons-dev (>= 5.51.0~),
               libkf5crash-dev (>= 5.51.0~),
               libkf5doctools-dev (>= 5.51.0~),
               libkf5i18n-dev (>= 5.51.0~),
               libkf5iconthemes-dev (>= 5.51.0~),
               libkf5kio-dev (>= 5.51.0~),
               libkf5parts-dev (>= 5.51.0~),
               libkf5service-dev (>= 5.51.0~),
               libkf5syntaxhighlighting-dev,
               libkf5texteditor-dev,
               libkf5wallet-dev,
               libkf5xmlgui-dev,
               libpoppler-qt5-dev,
               libqca-qt5-2-dev,
               libqt5networkauth5-dev,
               libqt5webkit5-dev [!amd64 !arm64 !armhf !i386],
               libqt5xmlpatterns5-dev,
               libxslt1-dev,
               pkg-kde-tools,
               pkgconf,
               qtbase5-dev,
               qtwebengine5-dev (>= 5.9.0~) [amd64 arm64 armhf i386],
               shared-mime-info
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/kbibtex
Vcs-Git: https://salsa.debian.org/science-team/kbibtex.git
Homepage: https://userbase.kde.org/KBibTeX
Rules-Requires-Root: no

Package: kbibtex
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: biber, texlive-bibtex-extra
Suggests: bibtex2html, latex2rtf, texlive-latex-base
Description: BibTeX editor for KDE
 An application to manage bibliography databases in the BibTeX format. KBibTeX
 can be used as a standalone program, but can also be embedded into other KDE
 applications (e.g. as bibliography editor into Kile).
 .
 KBibTeX can query online resources (e.g. Google scholar) via customizable
 search URLs. It is also able to import complete datasets from NCBI Pubmed.
 It also supports tagging references with keywords and manages references to
 local files.
 .
 BibTeX files can be exported into HTML, XML, PDF, PS and RTF format using a
 number of citation styles.
